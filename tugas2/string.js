//soal no 1
var word = 'JavaScript '; 
var second = 'is '; 
var third = 'awesome '; 
var fourth = 'and '; 
var fifth = 'I '; 
var sixth = 'love '; 
var seventh = 'it! ';
console.log(word.concat(second).concat(third).concat(fourth).concat(fifth).concat(sixth).concat(seventh))

// soal no 2
var sentence = "I am going to be React Native Developer"; 

var FirstWord = sentence[0] ; 
var SecondWord = sentence[2] + sentence[3]  ; 
var thirdWord = sentence[5]  + sentence[6] + sentence[7]+ sentence[8] + sentence[9]; // lakukan sendiri 
var fourthWord = sentence[11] + sentence[12] ; // lakukan sendidri 
var fifthWord = sentence[14] + sentence[15]; // lakukan sendiri 
var sixthWord = sentence[17] + sentence[18]+ sentence[19]+ sentence[20]+ sentence[21] ; // lakukan sendiri 
var seventhWord; // lakukan sendiri 
var eighthWord; // lakukan sendiri 

console.log('First Word: ' + FirstWord); 
console.log('Second Word: ' + SecondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord)


// soal no 3
var sentence2 = 'wow JavaScript is so cool'; 

var FirstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substring(4, 14); // do your own! 
var thirdWord2 = sentence2.substring(15, 17); // do your own! 
var fourthWord2 = sentence2.substring(18, 20); // do your own! 
var fifthWord2 = sentence2.substring(21, 25); // do your own! 

console.log('First Word: ' + FirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2)

//soal no 4
var sentence3 = 'wow JavaScript is so cool'; 

var exampleFirstWord3 = sentence3.substring(0, 3); 
var secondWord3 = sentence3.substring(4, 14); 
var thirdWord3 = sentence3.substring(15, 17);
var fourthWord3 = sentence3.substring(18, 20);  
var fifthWord3 = sentence3.substring(21, 25); // do your own! 

var firstWordLength = exampleFirstWord3.length  
var secondWordLength = secondWord3.length
var thirdWordLength = thirdWord3.length 
var fourthWordLength = fourthWord3.length
var fifthWordLength = fifthWord3.length


console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWordLength); 
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWordLength); 
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWordLength); 
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWordLength); 

